var app = new Vue({
    el:"#app",
    data:{
        products:[
            {id:1, title:"Mushroom 1", short_text:'Boletus edulis', image:'img/grib1.jpg', desc:"Full desc"},
            {id:2, title:"Mushroom 2", short_text:'Cortinarius caperatus', image:'img/grib2.png', desc:"Full desc"},
            {id:3, title:"Mushroom 3", short_text:'Lactarius deliciosus', image:'img/grib3.jpg', desc:"Full desc"},
            {id:4, title:"Mushroom 4", short_text:'Coprinus comatus', image:'img/grib4.jpeg', desc:"Full desc"},
            {id:5, title:"Mushroom 5", short_text:'Agaricus sylvaticus', image:'img/grib5.jpg', desc:"Full desc"}
        ],
        product:[],
        btnVisible: 0,
    },
  });