var app = new Vue({
    el:"#app",
    data:{
        products:[
            {id:1, title:"Mushroom 1", short_text:'Boletus edulis', image:'img/grib1.jpg', desc:"Full desc"},
            {id:2, title:"Mushroom 2", short_text:'Cortinarius caperatus', image:'img/grib2.png', desc:"Full desc"},
            {id:3, title:"Mushroom 3", short_text:'Lactarius deliciosus', image:'img/grib3.jpg', desc:"Full desc"},
            {id:4, title:"Mushroom 4", short_text:'Coprinus comatus', image:'img/grib4.jpeg', desc:"Full desc"},
            {id:5, title:"Mushroom 5", short_text:'Agaricus sylvaticus', image:'img/grib5.jpg', desc:"Full desc"}
        ],
        product:null,
        btnVisible: 0,

    },
    methods:{
        getProduct: function () {
            if(window.location.hash) {
                let id = window.location.hash.replace('#', '');
                for(let i in this.products) {
                    if(id==this.products[i].id){
                        this.product=this.products[i];
                        this.btnVisible = 1;
                        console.log(this.product);
                    }
                }
               
            }
        },
        addToCart: function(id) {
            let cart = [];
            if(window.localStorage.getItem('cart')) {
                cart = window.localStorage.getItem('cart').split(',');
                this.btnVisible = 0;
            }
            else{
                window.localStorage.setItem('cart', []);
            }
            
            if(cart.indexOf(String(id))==-1) {
                cart.push(id);
                window.localStorage.setItem('cart', cart.join());
                this.btnVisible = 1;
            }
        },
        checkInCart: function(){
            let itemPresent = window.localStorage.getItem('cart').split(',').indexOf(String(this.product.id));
            if(this.product && this.product.id && itemPresent!=-1){
                this.btnVisible = 1;
            }
            else{
                this.btnVisible = 0;
            }
        },
    },
    beforeMount(){
      this.getProduct();
      this.checkInCart();
    },

  });

