var app = new Vue({
    el:"#app",
    data:{
        products:[
            {id:1, title:"Mushroom 1", short_text:'Boletus edulis', image:'img/grib1.jpg', desc:"Full desc"},
            {id:2, title:"Mushroom 2", short_text:'Cortinarius caperatus', image:'img/grib2.png', desc:"Full desc"},
            {id:3, title:"Mushroom 3", short_text:'Lactarius deliciosus', image:'img/grib3.jpg', desc:"Full desc"},
            {id:4, title:"Mushroom 4", short_text:'Coprinus comatus', image:'img/grib4.jpeg', desc:"Full desc"},
            {id:5, title:"Mushroom 5", short_text:'Agaricus sylvaticus', image:'img/grib5.jpg', desc:"Full desc"}
        ],
        cart:[],
        contactFields:{
            name:null,
            companyName:null,
            position:null,
            city:null,
            country:null,
            email:null,
            role:null,
            other:null,
            comment:null,
        },
        order:[],
        formShow: 0,
    },
    methods:{
        getCart: function () {
            let localArray = null;
            localArray = String(window.localStorage.getItem('cart')).split(',');
            for(let i=0; i<localArray.length; i++){
                for(let j=0; j<this.products.length; j++){
                    if(localArray[i] == this.products[j].id){
                        this.cart.push(this.products[j]);
                        console.log(this.cart);
                    }
                }
            }

        },

        removeFromCart: function (id) {
            let indexDelItem = 0;
            let localArray = window.localStorage.getItem('cart').split(",");
            
            for (let i in localArray) {
                if(id == localArray){
                    indexDelItem = i;
                }
            }

            localArray.splice(indexDelItem,1);
            window.localStorage.setItem('cart', localArray);

            for (let i in this.cart) {
                if(id == this.cart[i].id){
                    indexDelItem = i;
                }
            }

            this.cart.splice(indexDelItem,1);

        },

        makeOrder: function () {
            this.formShow = 1;
            window.localStorage.setItem('cart', "");
            console.log(this.contactFields);

        }

    },
    mounted(){
      this.getCart();
    },

  });